<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Middleware\Logging;
$app = new \Slim\App();
$app-> add (new Logging());
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('Hello customer number '.$args['number']);
});
$app->get('/customers/{number}/products/{productnumber}', function($request, $response,$args){
   return $response->write('Hello customer number '.$args['number'].' you have product number '.$args['productnumber'].' in your cart');
});
$app->get('/messages', function($request, $response,$args){
  $_message = new Message();
  $messages = $_message->all();
  $payload = [];
  foreach($messages as $msg){
    $payload[$msg->id] = [
    'body'=>$msg->body,
    'user_id'=> $msg->user_id,
    'created_at'=>$msg->created_at
    ];
  }
  return $response->withStatus(200)->withJson($payload);
});
$app->post('/messages',function($request, $response,$args){
$message = $request->getParsedBodyParam('message','');
$userid = $request->getParsedBodyParam('userid','');
$_message = new Message(); 
$_message->body =$message;
$_message->user_id=$userid;
$_message->save();
if($_message->id){
    $payload = ['message_id'=>$_message->id];
    return $response->withStatus(201)->withJson($payload);
    } 
else
    {
    return $response->withStatus(400);
    }

});
$app->delete('/messages/{message_id}', function($request, $response,$args){
  $message = Message::find($args['message_id']);
  $message->delete();
  if ($message->exist){
    return $response->withStatus(400);
  }
  else {
    return $response->withStatus(200);
  }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
  $message = $request->getParsedBodyParam('message','');
  $_message = Message::find($args['message_id']);
  $_message->body = $message;
  if ($_message->save()){
    $payload = ['massage_id'=>$_message->id,"result"=>"The message has been updated succesfuly"];
    return $response->withStatus(200)->withJson($payload);
  }
  else {
    return $response->withStatus(400);
  }
});
$app->post('/messages/bulk',function($request, $response,$args){
  $payload = $request->getParsedBody();
  Message::insert($payload);
  return $response->withStatus(201)->withJson($payload);
});

$app->get('/users', function($request, $response,$args){
  $_user = new User();
  $users = $_user->all();
  $payload = [];
  foreach($users as $usr){
    $payload[$usr->id] = [
    'username'=>$usr->username,
    'password'=> $usr->password,
    'email'=>$usrmsg->email
    ];
  }
  return $response->withStatus(200)->withJson($payload);
});

$app->post('/users',function($request, $response,$args){
$user = $request->getParsedBodyParam('user','');
$userid = $request->getParsedBodyParam('userid','');
$_user = new User(); 
$_user->username =$username;
$_user->id=$userid;
$_user->save();
if($_user->id){
    $payload = ['user_id'=>$_user->id];
    return $response->withStatus(201)->withJson($payload);
    } 
else
    {
    return $response->withStatus(400);
    }

});

$app->post('/users/bulk',function($request, $response,$args){
  $payload = $request->getParsedBody();
  User::insert($payload);
  return $response->withStatus(201)->withJson($payload);
});

$app->delete('/users/{user_id}', function($request, $response,$args){
  $user = User::find($args['user_id']);
  $user->delete();
  if ($user->exist){
    return $response->withStatus(400);
  }
  else {
    return $response->withStatus(200);
  }
});

$app->put('/users/{user_id}', function($request, $response,$args){
  $user = $request->getParsedBodyParam('user','');
  $_user = User::find($args['user_id']);
  $_user->username = $user;
  if ($_user->save()){
    $payload = ['user_id'=>$_user->id,"result"=>"The username has been updated succesfuly"];
    return $response->withStatus(200)->withJson($payload);
  }
  else {
    return $response->withStatus(400);
  }
});





$app->run();
